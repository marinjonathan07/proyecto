// Acceder a los formularios y los campos de entrada
const registerForm = document.querySelector('#register-form');
const loginForm = document.querySelector('#login-form');
const usernameInput = document.querySelector('#username');
const emailInput = document.querySelector('#email');
const passwordInput = document.querySelector('#password');
const confirmPasswordInput = document.querySelector('#confirm-password');
const loginUsernameInput = document.querySelector('#login-username');
const loginPasswordInput = document.querySelector('#login-password');

// Manejar el evento de registro
registerForm.addEventListener('submit', function(e) {
  e.preventDefault();
  if (passwordInput.value !== confirmPasswordInput.value) {
  alert('Las contraseñas no coinciden');
  return;
  }
  alert('Registro exitoso!');
  // Limpiar los campos del formulario
  usernameInput.value = '';
  emailInput.value = '';
  passwordInput.value = '';
  confirmPasswordInput.value = '';
});

// Manejar el evento de inicio de sesión
loginForm.addEventListener('submit', function(e) {
  e.preventDefault();
});


